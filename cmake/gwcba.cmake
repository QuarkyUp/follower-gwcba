include_guard()
include(FetchContent)

FetchContent_Declare(
        gwcba
        GIT_REPOSITORY https://gitlab.com/QuarkyUp/gw-client-bot-api.git
        GIT_TAG e634d8e613890acbcf24eefcb5cf6445a563922e
)
FetchContent_MakeAvailable(gwcba)
