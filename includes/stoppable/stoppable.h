#ifndef STOPPABLE_H
#define STOPPABLE_H

#include <algorithm>
#include <chrono>
#include <future>
#include <iostream>

class Stoppable {
  std::promise<void> exitSignal;
  std::future<void> futureObj;

 protected:
  bool isRunning;

 public:
  bool getIsRunning() const { return isRunning; }

 public:
  Stoppable() : futureObj(exitSignal.get_future()), isRunning(false) {}
  Stoppable(Stoppable&& obj) : exitSignal(std::move(obj.exitSignal)), futureObj(std::move(obj.futureObj)) {
    std::cout << "Move Constructor is called" << std::endl;
  }
  Stoppable& operator=(Stoppable&& obj) {
    std::cout << "Move Assignment is called" << std::endl;
    exitSignal = std::move(obj.exitSignal);
    futureObj = std::move(obj.futureObj);
    return *this;
  }

  virtual void run() {}

  void operator()() { run(); }

  bool stopRequested() {
    if (futureObj.wait_for(std::chrono::milliseconds(0)) == std::future_status::timeout) return false;
    return true;
  }

  void stop() {
    try {
      exitSignal.set_value();
    } catch (...) {
    }
  }
};

#endif
