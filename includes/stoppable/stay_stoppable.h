#ifndef STAY_STOPPABLE_H
#define STAY_STOPPABLE_H

#include <Windows.h>
#include <gwcba/agents.h>
#include <gwcba/gwcba.h>
#include <gwcba/movement.h>
#include <gwcba/travel.h>
#include <gwcba/utils.h>
#include <ipc/ipc.h>
#include <stoppable/stoppable.h>

class StayTask : public Stoppable {
 public:
  void run() {
    std::cout << "Stay Task Start" << std::endl;
    this->isRunning = true;
    while (!stopRequested()) Sleep(50);
    this->isRunning = false;
    std::cout << "Stay Task End" << std::endl;
  }
};

#endif
