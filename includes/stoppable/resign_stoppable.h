#ifndef RESIGN_STOPPABLE_H
#define RESIGN_STOPPABLE_H

#include <Windows.h>
#include <gwcba/agents.h>
#include <gwcba/gwcba.h>
#include <gwcba/movement.h>
#include <gwcba/travel.h>
#include <gwcba/utils.h>
#include <ipc/ipc.h>
#include <stoppable/stoppable.h>

class ResignTask : public Stoppable {
 public:
  void run() {
    std::cout << "Resign Task Start" << std::endl;
    this->isRunning = true;
    GW::Chat::SendChat('/', "resign");
    this->isRunning = false;
    std::cout << "Resign Task End" << std::endl;
  }
};

#endif
