#ifndef FOLLOW_STOPPABLE_H
#define FOLLOW_STOPPABLE_H

#include <Windows.h>
#include <gwcba/agents.h>
#include <gwcba/gwcba.h>
#include <gwcba/movement.h>
#include <gwcba/travel.h>
#include <gwcba/utils.h>
#include <ipc/ipc.h>
#include <stoppable/stoppable.h>

class FollowTask : public Stoppable {
 public:
  void run() {
    std::cout << "Follow Task Start" << std::endl;

    std::vector<std::wstring> args = {};

    GW::Agent* agentToFollow = nullptr;
    const auto partyLeader = getPartyLeader();
    if (!partyLeader) return;

    if (args.empty() || args[0].find(L"leader") != std::string::npos) {
      agentToFollow = GW::Agents::GetAgentByID(partyLeader->agent_id);
    } else if (args[0].find(L"target") != std::string::npos) {
      IPC ipc = IPC(std::string("Global\\gw_ipc"));
      ipc.open();
      auto data = ipc.receive();
      if (data.leader.currentTarget.agentId == 0) {
        agentToFollow = GW::Agents::GetAgentByID(partyLeader->agent_id);
      } else {
        agentToFollow = GW::Agents::GetAgentByID(data.leader.currentTarget.agentId);
      }
      ipc.close();
    }

    this->isRunning = true;
    while (!stopRequested()) {
      if (!agentToFollow) break;
      if (!waitForCurrentMapLoaded()) break;

      if (!moveToMovingAgent(agentToFollow, [&]() {
            if (!agentToFollow) return false;
            if (!waitForCurrentMapLoaded()) return false;
            if (this->stopRequested()) return false;
            return true;
          }))
        break;
    }
    this->isRunning = false;
    std::cout << "Follow Task End" << std::endl;
  }
};

#endif
