#ifndef COMMANDPARSER_H
#define COMMANDPARSER_H

#include <string>
#include <vector>

wchar_t* GetMessageCore();
std::wstring sanitizeLastMessage();
std::vector<std::wstring> split(const std::wstring& s, const wchar_t* delimiter);

#endif
