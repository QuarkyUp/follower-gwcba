#ifndef AVAILABLE_COMMAND_ENUM_H
#define AVAILABLE_COMMAND_ENUM_H

#include <string>

enum AvailableCommandEnum { FOLLOW, STAY, RESIGN, MOVE, NONE, LENGTH };

AvailableCommandEnum getCommandEnumValueFromName(const std::wstring& commandName);

#endif
