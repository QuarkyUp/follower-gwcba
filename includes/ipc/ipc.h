#ifndef IPC_H
#define IPC_H

#include <Windows.h>

#include "nlohmann/json.hpp"
#include <string>

using nlohmann::json;

struct GWSharedData {
  struct Leader {
    struct Position {
      float x;
      float y;

      struct Map {
        uint32_t id;
        uint32_t districtId;
        uint32_t regionId;
        uint32_t languageId;
      } map;
    } position;

    struct CurrentTarget {
      uint32_t agentId;
    } currentTarget;

    uint32_t lastDialogId;
  } leader;
};

void from_json(const json &j, GWSharedData &p);

class IPC {
 public:
  IPC(std::string mapFileName);

  bool create();

  bool open();

  bool close();

  bool send(std::string data);

  [[nodiscard]] GWSharedData receive();

 private:
  const uint32_t maxBufferSize = 1024;
  std::string mapFileName;
  HANDLE mapFileHandle;
  LPCTSTR mapFileAddress;

  bool closeHandle();

  bool unmapFile();
};

#endif
