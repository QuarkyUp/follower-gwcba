#include <command/available_command_enum.h>

#include <map>
#include <string>

AvailableCommandEnum getCommandEnumValueFromName(const std::wstring& commandName) {
  const std::map<std::wstring, AvailableCommandEnum> table{
      {L"follow", AvailableCommandEnum::FOLLOW},
      {L"stay", AvailableCommandEnum::STAY},
      {L"resign", AvailableCommandEnum::RESIGN},
      {L"move", AvailableCommandEnum::MOVE},
  };

  auto it = table.find(commandName);
  if (it != table.end()) return it->second;
  return AvailableCommandEnum::NONE;
}
