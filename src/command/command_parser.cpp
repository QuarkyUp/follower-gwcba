#include <command/command_parser.h>
#include <gwcba/gwcba.h>

wchar_t *GetMessageCore() {
  GW::Array<wchar_t> *buff = &GW::GameContext::instance()->world->message_buff;
  return buff ? buff->begin() : nullptr;
}

std::wstring sanitizeLastMessage() {
  auto message = std::wstring(GetMessageCore());

  auto prefix = std::wstring(L"\x108\x107");
  auto suffix = std::wstring(L"\x1");

  size_t start_idx = message.find(prefix);
  start_idx += 2;
  if (start_idx == std::wstring::npos) return {};

  size_t end_idx = message.find(suffix, start_idx);
  if (end_idx == std::wstring::npos) return {};

  auto sanitized = message.substr(start_idx, end_idx - 2);
  return sanitized;
}

std::vector<std::wstring> split(const std::wstring &s, const wchar_t *delimiter) {
  std::wstring temp;
  std::vector<std::wstring> parts;
  std::wstringstream wss(s);

  while (std::getline(wss, temp, *delimiter)) parts.push_back(temp);

  return parts;
}