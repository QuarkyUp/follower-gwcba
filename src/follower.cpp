#include <command/command_parser.h>
#include <follower.h>
#include <gwcba/bot.h>
#include <gwcba/gwcba.h>

#include <chrono>
#include <future>
#include <memory>
#include <thread>
using namespace std::chrono_literals;
#include <stoppable/follow_stoppable.h>
#include <stoppable/resign_stoppable.h>
#include <stoppable/stay_stoppable.h>
#include <command/available_command_enum.h>

bool compareVectors(std::vector<std::wstring> vec1, std::vector<std::wstring> vec2) {
  for (const auto &v1item : vec1) {
    for (const auto &v2item : vec2) {
      if (v1item == v2item) return true;
    }
  }
  return false;
}

void cancelRunningTask(bool &wasLoading, Stoppable *currentTask, AvailableCommandEnum &receivedCommandType) {
  if (currentTask) {
    currentTask->stop();
    while (currentTask->getIsRunning()) Sleep(50);
    receivedCommandType = NONE;
    wasLoading = false;
  }
}

bool run() {
  std::mutex commandTypeMutex;

  GW::HookEntry ChatMessageCallback_Entry;
  GW::HookEntry InstanceLoadInfo_Entry;

  botRunning = true;

  std::vector<std::wstring> commandArguments;
  AvailableCommandEnum receivedCommandType = NONE;
  Stoppable *currentTask = nullptr;
  bool wasLoading = false;

  GW::StoC::RegisterPacketCallback<GW::Packet::StoC::MessageLocal>(
      &ChatMessageCallback_Entry, [&](GW::HookStatus *status, GW::Packet::StoC::MessageLocal *pak) {
        if (GW::PartyMgr::GetPartySize() == 1 || GW::PartyMgr::GetPartyPlayerCount() == 1) return;

        const auto channelEnum = static_cast<GW::Chat::Channel>(pak->channel);
        if (channelEnum != GW::Chat::Channel::CHANNEL_GROUP) return;

        const auto sanitizedCommand = sanitizeLastMessage();
        if (sanitizedCommand.empty()) return;

        const auto commandLineWithArgsSplit = split(sanitizedCommand, L" ");
        const auto &commandName = commandLineWithArgsSplit.at(0);
        commandArguments = std::vector<std::wstring>(commandLineWithArgsSplit.begin() + 1, commandLineWithArgsSplit.end());

        receivedCommandType = getCommandEnumValueFromName(commandName);
      });

  GW::StoC::RegisterPacketCallback<GW::Packet::StoC::InstanceLoadInfo>(
      &InstanceLoadInfo_Entry, [&](GW::HookStatus *status, GW::Packet::StoC::InstanceLoadInfo *packet) -> void { wasLoading = true; });

  std::unique_ptr<std::thread> thread_ptr;

  while (botRunning) {
    if (wasLoading) cancelRunningTask(wasLoading, currentTask, receivedCommandType);
    if (receivedCommandType == NONE) continue;

    switch (receivedCommandType) {
      case FOLLOW: {
        cancelRunningTask(wasLoading, currentTask, receivedCommandType);
        currentTask = new FollowTask();
        break;
      }

      case STAY: {
        cancelRunningTask(wasLoading, currentTask, receivedCommandType);
        currentTask = new StayTask();
        break;
      }

      case RESIGN: {
        cancelRunningTask(wasLoading, currentTask, receivedCommandType);
        currentTask = new ResignTask();
        break;
      }
    }

    if (!currentTask) continue;

    thread_ptr = std::make_unique<std::thread>([&]() {
      while (!currentTask->stopRequested()) currentTask->run();
    });
    thread_ptr->detach();
  }

  GW::StoC::RemoveCallback<GW::Packet::StoC::InstanceLoadInfo>(&InstanceLoadInfo_Entry);
  GW::StoC::RemoveCallback<GW::Packet::StoC::MessageLocal>(&ChatMessageCallback_Entry);

  return true;
}
