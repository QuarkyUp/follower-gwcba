#include <ipc/ipc.h>

#include <utility>

IPC::IPC(std::string mapFileName) : mapFileName(std::move(mapFileName)) {}

bool IPC::create() {
  mapFileHandle = CreateFileMapping(INVALID_HANDLE_VALUE, nullptr, PAGE_READWRITE, 0, this->maxBufferSize, this->mapFileName.c_str());

  if (!mapFileHandle) {
    this->closeHandle();
    return false;
  }

  mapFileAddress = static_cast<LPCTSTR>(MapViewOfFile(mapFileHandle, FILE_MAP_ALL_ACCESS, 0, 0, this->maxBufferSize));
  if (!mapFileAddress) {
    this->unmapFile();
    return false;
  }

  return true;
}

bool IPC::open() {
  mapFileHandle = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, this->mapFileName.c_str());
  if (!mapFileHandle) {
    this->closeHandle();
    return false;
  }

  mapFileAddress = static_cast<LPCTSTR>(MapViewOfFile(mapFileHandle, FILE_MAP_ALL_ACCESS, 0, 0, this->maxBufferSize));
  if (!mapFileAddress) {
    this->unmapFile();
    return false;
  }

  return true;
}

bool IPC::close() { return this->closeHandle() && this->unmapFile(); }

bool IPC::send(std::string data) {
  if (!this->mapFileHandle || !this->mapFileAddress) return false;

  CopyMemory((PVOID)mapFileAddress, data.data(), (long)((data.size() + 1)));

  return true;
}

GWSharedData IPC::receive() {
  if (!this->mapFileHandle || !this->mapFileAddress) return {};

  auto temp = std::make_unique<char[]>(maxBufferSize);
  memcpy(temp.get(), this->mapFileAddress, maxBufferSize);

  auto dataStr = std::string(temp.get());
  json jsonData = json::parse(dataStr);

  return jsonData.at("data").get<GWSharedData>();
}

bool IPC::closeHandle() { return CloseHandle(this->mapFileHandle); }

bool IPC::unmapFile() { return UnmapViewOfFile(this->mapFileAddress); }

void from_json(const json& j, GWSharedData& p) {
  j.at("leader").at("position").at("x").get_to(p.leader.position.x);
  j.at("leader").at("position").at("y").get_to(p.leader.position.y);

  j.at("leader").at("position").at("map").at("id").get_to(p.leader.position.map.id);
  j.at("leader").at("position").at("map").at("districtId").get_to(p.leader.position.map.districtId);
  j.at("leader").at("position").at("map").at("regionId").get_to(p.leader.position.map.regionId);
  j.at("leader").at("position").at("map").at("languageId").get_to(p.leader.position.map.languageId);

  j.at("leader").at("currentTarget").at("agentId").get_to(p.leader.currentTarget.agentId);

  j.at("leader").at("lastDialogId").get_to(p.leader.lastDialogId);
}
